<?php

namespace App\Repository;

use App\Models\Comment;
use App\Resources\CommentResource;

class CommentRepository
{
    public function searchFromRequest()
    {
        $articles = Comment::query();
        return CommentResource::collection($articles->get());
    }
}
