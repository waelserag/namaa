<?php

namespace App\Services;

use App\Models\Comment;

class CommentService
{

    /**
     * Execute the comment process
     *
     * @param array $request
     * @param int $article_id
     * @return Comment
     */
    public function execute(array $request, int $article_id): Comment
    {
        $request['article_id'] = $article_id;
        return $this->createOrUpdateComment($request);
    }

    /**
     * Create or update the comment
     *
     * @param array $request
     * @return Comment
     */
    private function createOrUpdateComment(array $request): Comment
    {   
        $request['user_id'] = auth()->id();
        $comment =  Comment::create($request);
        return $comment;
    }
    
}