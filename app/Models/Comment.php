<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "comment",
        "article_id",
        "user_id"
    ];

    /**
     * Get the user associated with the comment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The user relation.
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the article associated with the comment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The article relation.
     */
    public function article(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Article::class);
    }

}
