<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the user's profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit() :view
    {
        $user = User::findOrFail(auth()->id());
        $title = trans('admin.edit_profile');
        return view("admin.user.form", compact("user", "title"));
    }
}