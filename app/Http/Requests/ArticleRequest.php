<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'title'   => 'required|string|min:5|max:190',
            'content' => 'required|string|min:5',
            'status'  => 'nullable|in:0,1',
        ];
    }

}
