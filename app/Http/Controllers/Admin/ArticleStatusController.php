<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\RedirectResponse;

class ArticleStatusController extends Controller
{
    /**
     * Change article to active
     * 
     * @param int $article_id
     *
     * @return RedirectResponse
     */
    public function update(int $article_id) :RedirectResponse
    {
        Article::where("id", $article_id)->update(["status"=> 1]);
        // Redirect back
        return redirect()->route('admin.articles.index');
    }
}