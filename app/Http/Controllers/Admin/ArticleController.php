<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Repository\ArticleRepository;
use App\Services\ArticleService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(ArticleRepository $articleRepository): View
    {
        // Get all articles and transform them using the ArticleResource
        $articles = $articleRepository->searchFromRequest(request());
        $title = trans('admin.articles');

        // Pass the articles and title to the view
        return view("admin.article.index", compact("articles", "title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        // Initialize article and title variables
        $article = null;
        $title = trans('admin.add article');

        // Pass the article and title to the view
        return view("admin.article.form", compact("article", "title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ArticleRequest  $request
     * @param  \App\Services\ArticleService  $articleService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ArticleRequest $request, ArticleService $articleService): RedirectResponse
    {
        // Create a new article using the validated data
        $articleService->execute($request->validated());

        // Flash a success message to the session
        session()->flash('success', trans("admin.add Successfully"));

        // Redirect the user to the create form again
        return redirect()->route('admin.articles.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\View\View
     */
    public function edit(Article $article): View
    {
        // Set the title for the edit form
        $title = trans('admin.edit article');

        // Pass the article and title to the view
        return view("admin.article.form", compact("article", "title"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Article  $article
     * @param  \App\Http\Requests\ArticleRequest  $request
     * @param  \App\Services\ArticleService  $articleService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Article $article, ArticleRequest $request, ArticleService $articleService): RedirectResponse
    {
        // Update the article with the validated data
        $articleService->execute($request->validated(), $article);

        // Flash a success message to the session
        session()->flash('success', trans("admin.edit Successfully"));

        // Redirect the user back to the previous page
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  \App\Services\ArticleService  $articleService
     * @return void
     */
    public function destroy(int $id, ArticleService $articleService)
    {
        // Delete the article using the ArticleService
        return $articleService->delete($id);
    }
}