<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class ArticleSearchTest extends TestCase
{
    protected function authenticateUser(): void
    {
        Auth::loginUsingId(1);
    }

    public function testSearchByTitle()
    {
        $this->authenticateUser();

        // Create a test article
        $article = Article::create([
            "title"   => 'Test Article',
            "content" => "test content",
            "user_id" => auth()->id(),
        ]);

        // Perform the search
        $response = $this->get('/admin/articles?filter[title]=Test Article');

        // Assert that the response contains the article
        $response->assertSee($article->title);
    }

    public function testSearchByContent()
    {
        $this->authenticateUser();
        
        // Create a test article
        $article = Article::create([
            "title"   => 'Test Article2',
            "content" => "test content2",
            "user_id" => auth()->id(),
        ]);

        // Perform the search
        $response = $this->get('/admin/articles?filter[content]=test content2');

        // Assert that the response contains the article
        $response->assertSee($article->content);
    }
}
