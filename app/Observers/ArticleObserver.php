<?php

namespace App\Observers;

use App\Jobs\NotifyAdmins;
use App\Models\Article;
use Illuminate\Support\Facades\Cache;

class ArticleObserver
{
    /**
     * Handle the Article "created" event.
     */
    public function created(): void
    {
        NotifyAdmins::dispatch()->onQueue('emails');
        Cache::forget('articles');
    }
    
    /**
     * Handle the Article "updated" event.
     */
    public function updated(): void
    {
        Cache::forget('articles');
    }

    /**
     * Handle the Article "deleted" event.
     */
    public function deleted(): void
    {
        Cache::forget('articles');
    }
}
