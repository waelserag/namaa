@extends('admin.layouts.app')


@section('content')
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">{{ $title }}</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ aurl("/users/create") }}">{{ trans('admin.add user') }}</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- HTML5 export buttons table -->
      <!-- Column selectors table -->
      <section id="column-selectors">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{ $title }}</h4>
                <form id="search" method="get" >
                  <br />
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">
                        <label for="name">{{ trans('validation.attributes.name') }}</label>
                        <input type="text" id="name" placeholder="{{ trans('validation.attributes.name') }}" value="{{ request()->input('filter.name') ?? '' }}" class="form-control" style="display:inline;" name="filter[name]" />                  
                      </div>
                      <div class="col-md-2">
                        <label for="email">{{ trans('validation.attributes.email') }}</label>
                        <input type="text" id="email" placeholder="{{ trans('validation.attributes.email') }}"  value="{{ request()->input('filter.email') ?? '' }}" class="form-control" style="display:inline;" name="filter[email]" />
                      </div>
                      <div class="col-md-2">
                        <label for="roles">{{ trans('admin.roles') }}</label>
                        <select name="filter[role_id]" class="form-control" id="roles">
                          <option value="">{{ trans('admin.choose') }}</option>
                          <option value="1" {{ request()->input('filter.role_id') == 1 ? "selected" : ""   }}>{{ trans("permission.admin") }}</option>
                          <option value="2" {{ request()->input('filter.role_id') == 2 ? "selected" : ""   }}>{{ trans("permission.reviewer") }}</option>
                          <option value="3" {{ request()->input('filter.role_id') == 3 ? "selected" : ""   }}>{{ trans("permission.user") }}</option>
                        </select>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-success btn-min-width mr-1 mb-1" type="submit" style="display:inline; margin:25px auto;">{{ trans('admin.search') }}</button>
                      </div>
                      <div class="col-md-2">
                        <br>
                        <a class="btn btn-success btn-min-width mr-1 mb-1" href="{{ route("users.index") }}" style="display:block; margin:3px;">{{ trans('admin.reset_search') }}</a>
                      </div>
                    </div>
                  </div>
                </form>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  {{-- <table class="table table-striped table-bordered dataex-html5-selectors"> --}}
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>{{ trans('validation.attributes.name') }}</th>
                        <th>{{ trans('validation.attributes.email') }}</th>
                        <th>{{ trans('admin.roles') }}</th>
                        <th>{{ trans('admin.created_at') }}</th>
                        <th>{{ trans('admin.actions') }}</th>

                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr id="show_{{ $user->id }}">
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ \App\Models\User::getListRoles($user->role_id) }}</td>
                                <td>{{ $user->created_at ? date("Y-m-d H:i", strtotime($user->created_at)) : "" }}</td>
                                <td>
                                  @if ($user->role_id != 4)
                                  <a href="{{ aurl('/users/'.$user->id."/edit") }}" class="btn btn-secondary btn-min-width mr-1 mb-1"><i
                                      class="ft-edit"></i> {{ trans('admin.edit') }}</a>
                                  @endif
                                    <input type="hidden" value="{{ csrf_token() }}" id="csrf_token" />
                                    <button class="btn btn-danger btn-min-width btn-glow mr-1 mb-1" onclick="DeleteRow({{$user->id}})"><i
                                            class="ft-delete"></i> {{ trans('admin.delete') }}</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
          <div class="pagination" style="margin:10px auto">
            {{ $users->appends(request()->except('page'))->render('vendor.pagination.bootstrap-4') }}
          </div>

        </div>
      </section>
      <!--/ Column selectors table -->
</div>


@endsection
@section('scripts')
<script>
  function DeleteRow(id) {
    	           event.preventDefault();
                   swal({
           		    title: "Are you sure?",
           		    text: "You will not be able to recover this imaginary file!",
           		    icon: "warning",
           		    buttons: {
                           cancel: {
                               text: "No, cancel",
                               value: null,
                               visible: true,
                               className: "",
                               closeModal: false,
                           },
                           confirm: {
                               text: "Yes, delete it!",
                               value: true,
                               visible: true,
                               className: "",
                               closeModal: false
                           }
           		    }
           		})
           		.then((isConfirm) => {
           		    if (isConfirm) {
                        $.ajax({
                            type: 'delete',
                             url: "{{aurl("/users/")}}"+'/'+id,
                            data: {
                                '_token': $('#csrf_token').val(),
                            },

                        });
                        $('#show_' + id).remove();
           		        swal("Deleted!", "Your data has been deleted.", "success");
           		    } else {
           		        swal("Cancelled", "Your data is safe", "error");
           		    }
           		});

            }

</script>
@endsection
