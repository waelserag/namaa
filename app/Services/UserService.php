<?php

namespace App\Services;

use App\Models\User;

class UserService
{

    /**
     * Execute the user process
     *
     * @param array $request
     * @param User|null $user
     * @return User
     */
    public function execute(array $request, User $user = null): User
    {
        return $this->createOrUpdateUser($request, $user);
    }

    /**
     * Create or update the user
     *
     * @param array $request
     * @param User|null $user
     * @return User
     */
    private function createOrUpdateUser(array $request, User $user = null): User
    {
        if (!isset($request["role_id"]) && $user) {
            $request["role_id"] = $user->role_id;
        }

        // If no user is provided, create a new user with the request data
        if (!$user) {
            $user =  user::create($request);
            return $user;
        }
        if (!$request['password']) {
            unset($request['password']);
        }
        // Otherwise, update the existing user with the request data
        $user->fill($request)->save();
        return $user;
    }

    /**
     * Delete User
     *
     * @param int $userId
     * @return void
     */
    public function delete(int $userId): void
    {
        $user = User::where('id', $userId)->firstOrFail();
        $user->delete();
    }
    
}