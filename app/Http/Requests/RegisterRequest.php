<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'email'    => 'required|email|unique:users,email',
            'name'     => 'required|string|min:7|max:190',
            'password' => 'required|string|min:6|max:25'
        ];
    }

}
