<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Repository\UserRepository;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a list of users.
     *
     * @param UserRepository $userRepository The user repository.
     * @return View The view displaying the list of users.
     */
    public function index(UserRepository $userRepository): View
    {
        // Retrieve users based on search criteria
        $users = $userRepository->searchFromRequest(request());
        $title = trans('admin.users');
        
        // Pass users and title to the view
        return view("admin.user.index", compact("users", "title"));
    }

    /**
     * Display the form for creating a new user.
     *
     * @return View The view displaying the user creation form.
     */
    public function create(): View
    {
        $user = null;
        $title = trans('admin.add user');
        
        // Pass user and title to the view
        return view("admin.user.form", compact("user", "title"));
    }

    /**
     * Store a newly created user in the database.
     *
     * @param UserStoreRequest $request The request containing the user data.
     * @param UserService $userService The user service.
     * @return RedirectResponse A redirect response to the user creation form.
     */
    public function store(UserStoreRequest $request, UserService $userService): RedirectResponse
    {
        // Execute the user creation logic
        $userService->execute($request->validated());
        
        // Flash a success message to the session
        session()->flash('success', trans("admin.add Successfully"));
        
        // Redirect to the create user form
        return redirect()->route('users.create');
    }

    /**
     * Display the form for editing an existing user.
     *
     * @param User $user The user to be edited.
     * @return View The view displaying the user edit form.
     */
    public function edit(User $user): View
    {
        $title = trans('admin.edit user');
        
        // Pass user and title to the view
        return view("admin.user.form", compact("user", "title"));
    }

    /**
     * Update the specified user in the database.
     *
     * @param User $user The user to be updated.
     * @param UserUpdateRequest $request The request containing the updated user data.
     * @param UserService $userService The user service.
     * @return RedirectResponse A redirect response back to the previous page.
     */
    public function update(User $user, UserUpdateRequest $request, UserService $userService): RedirectResponse
    {
        // Execute the user update logic
        $userService->execute($request->validated(), $user);
        
        // Flash a success message to the session
        session()->flash('success', trans("admin.edit Successfully"));
        
        // Redirect back to the previous page
        return redirect()->back();
    }

    /**
     * Remove the specified user from the database.
     *
     * @param int $id The ID of the user to be deleted.
     * @param UserService $userService The user service.
     * @return mixed The result of the user deletion operation.
     */
    public function destroy(int $id, UserService $userService)
    {
        // Delete the user
        return $userService->delete($id);
    }
}