<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Display the Dashboard Page.
     *
     * @return View
     */
    public function index()
    {
        return view('admin.index');
    }
}
