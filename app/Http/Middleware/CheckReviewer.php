<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckReviewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(Request): (Response)  $next
     */
    public function handle($request, Closure $next)
    {
        // Check if the user is authenticated and has the reviewer role
        if (auth()->check() && auth()->user()->role_id == 2) {
            // User is authenticated and has reviewer role, proceed with the request
            return $next($request);
        }
        
        // Check if the user is already authenticated
        if (auth()->check()) {
            // User is authenticated but does not have reviewer role, show an error message
            return redirect()->route('dashboard')->with('error', 'Unauthorized access.');
        }

        // User is not authenticated, redirect to the login page
        return redirect()->route('admin.login')->with('error', 'Unauthorized access.');
    }
}