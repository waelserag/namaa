<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(): View
    {
        // Get all articles and transform them using the ArticleResource
        $key = 'articles';

        $articles = Cache::get($key);

        if (!$articles) {
            $articles = Article::with("user", "comments")->active()->paginate(20);
            Cache::put($key, $articles);
        }

        $title = trans('admin.articles');

        // Pass the articles and title to the view
        return view("welcome", compact("articles", "title"));
    }
}
