<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'title'   => $this->title,
            'content' => $this->content,
            'status'  => $this->status,
            'user_id' => $this->user_id,
            'user'    => $this->whenLoaded('user', new UserResource($this->user)),
            'comments'=>  CommentResource::collection($this->whenLoaded('comments')),
            'created_at' => date("Y-m-d", strtotime($this->created_at)),
        ];
    }

}
