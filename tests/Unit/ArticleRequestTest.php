<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ArticleRequestTest extends TestCase
{

    public function testValidRequest()
    {
        Auth::loginUsingId(1);

        $data = [
            'title' => 'Valid Title',
            'content' => 'Valid Content',
            'status' => '1',
        ];

        $request = new ArticleRequest();
        $validator = Validator::make($data, $request->rules());

        $this->assertFalse($validator->fails());
    }

    public function testInvalidRequest()
    {
        Auth::loginUsingId(1);

        $data = [
            'title' => 'Short',
            'content' => 'Short',
            'status' => '2',
        ];

        $request = new ArticleRequest();
        $validator = Validator::make($data, $request->rules());
        $this->assertTrue($validator->fails());
    }
}
