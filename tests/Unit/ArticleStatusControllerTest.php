<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class ArticleStatusControllerTest extends TestCase
{
    public function testReviewer()
    {
        Auth::loginUsingId(2);
            
        // Create a test article
        $article = Article::create([
            "title"   => 'Test Article2',
            "content" => "test content2",
            "user_id" => auth()->id(),
        ]);

        // Call the update method
        $response = $this->get(route('admin.article.active', ['article_id' => $article->id]));

        // Assert that the article status is updated to active
        $this->assertEquals(1, $article->fresh()->status);

        // Assert that the response is a redirect
        $response->assertRedirect(route('admin.articles.index'));
    }

        public function testNormalUser()
    {
        Auth::loginUsingId(1);
            
        // Create a test article
        $article = Article::create([
            "title"   => 'Test Article2',
            "content" => "test content2",
            "user_id" => auth()->id(),
        ]);

        // Call the update method
        $response = $this->get(route('admin.article.active', ['article_id' => $article->id]));

        // Assert that the article status is updated to active
        $this->assertEquals(0, $article->fresh()->status);

        // Assert that the response is a redirect
        $response->assertRedirect(route('dashboard'));
    }
}
