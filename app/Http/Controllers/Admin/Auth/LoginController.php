<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LoginController extends Controller
{
    /**
     * Display the login form.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.auth.login');
    }

    /**
     * Handle the login request.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function login(Request $request): RedirectResponse
    {
        $credentials = $request->only('email', 'password');

        // Add 'remember' key to the credentials array
        $remember = $request->filled('remember') ?? false;

        if (Auth::attempt($credentials, $remember)) {
            // Authentication passed
            return redirect()->intended('admin/dashboard');
        }
        
        // Authentication failed
        return redirect()->back()->withErrors([
            'email' => trans("auth.failed"),
            ])->withInput($request->except('password'));
    }

    /**
     * Logout
     * 
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        auth()->logout();
        return redirect('/admin/login');
    }
}