<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Get the list of role with their translations.
     *
     * @param int|null $role The role key.
     * @param bool $admin to show admin or no.
     * 
     * @return array|string The list of role or the translation of a specific role.
     */
    public static function getListRoles(int $role = null, $admin = true)
    {
        if ($admin) {
            $items = [
                1 => trans("permission.admin"),
                2 => trans("permission.reviewer"),
                3 => trans("permission.user"),
            ];
        } else {
            $items = [
                2 => trans("permission.reviewer"),
                3 => trans("permission.user")
            ];
        }

        if (isset($items[$role])) {
            return $items[$role];
        } else {
            return $items;
        }
    }
}
