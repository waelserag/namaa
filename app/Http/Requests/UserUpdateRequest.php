<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'email'    => 'required|email|unique:users,email,'.$this->user->id,
            'name'     => 'required|string|min:7|max:190',
            'password' => 'sometimes|nullable|string|min:6|max:25',
            'role_id'  => 'required|in:1,2,3',
        ];
    }

}
