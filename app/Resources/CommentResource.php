<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'comment'   => $this->comment,
            'article_id'=> $this->article_id,
            'user_id'   => $this->user_id,
            'created_at'=> date("Y-m-d", strtotime($this->created_at)),
        ];
    }

}
