<?php

namespace App\Services;

use App\Models\Article;

class ArticleService
{

    /**
     * Execute the article process
     *
     * @param array $request
     * @param Article|null $article
     * @return Article
     */
    public function execute(array $request, Article $article = null): Article
    {
        return $this->createOrUpdateArticle($request, $article);
    }

    /**
     * Create or update the article
     *
     * @param array $request
     * @param Article|null $article
     * @return Article
     */
    private function createOrUpdateArticle(array $request, Article $article = null): Article
    {   
        // If no article is provided, create a new article with the request data
        if (!$article) {
            $request['user_id'] = auth()->id();
            $article =  Article::create($request);
            return $article;
        }

        // Otherwise, update the existing article with the request data
        $article->fill($request)->save();
        return $article;
    }

    /**
     * Delete Article
     *
     * @param int $articleId
     * @return void
     */
    public function delete(int $articleId): void
    {
        $articleQuery = Article::whereId($articleId);

        $article = $articleQuery->firstOrFail();
        $article->delete();
    }
    
}