<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repository\ArticleRepository;
use App\Traits\ApiResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * @group Articles
 *
 * Articles Apis
 */
class ArticleController extends Controller
{
    use ApiResponse;
    
    /**
     * Index
     * 
     * List of articles
     * 
     * @queryParam filter[title] string Filter by Title. Example: new article
     * @queryParam filter[content] string Filter by Title. Example: new content
     * 
     * @response 200 {"data":[{"id":1,"title":"Dolores corrupti en","content":"description","status":1,"user_id":3,"user":{"id":3,"name":"Victoria Wilkinson","email":"hycipicuj@mailinator.com","role_id":"User","created_at":"2023-11-11"},"comments":[{"id":1,"comment":"يسبيسشب يسشب يسشبيس","article_id":1,"user_id":8,"user":{"id":8,"name":"shady serag","email":"shady@admin.com","email_verified_at":null,"role_id":3,"created_at":"2023-11-12T17:17:38.000000Z","updated_at":"2023-11-12T17:17:38.000000Z"},"created_at":"2023-11-12"},{"id":2,"comment":"test test test","article_id":1,"user_id":8,"user":{"id":8,"name":"shady serag","email":"shady@admin.com","email_verified_at":null,"role_id":3,"created_at":"2023-11-12T17:17:38.000000Z","updated_at":"2023-11-12T17:17:38.000000Z"},"created_at":"2023-11-12"}],"created_at":"2023-11-11"},{"id":2,"title":"Suscipit qui quos si","content":"test test test","status":0,"user_id":3,"user":{"id":3,"name":"Victoria Wilkinson","email":"hycipicuj@mailinator.com","role_id":"User","created_at":"2023-11-11"},"comments":[{"id":3,"comment":"aaaa bbbb ccc","article_id":2,"user_id":8,"user":{"id":8,"name":"shady serag","email":"shady@admin.com","email_verified_at":null,"role_id":3,"created_at":"2023-11-12T17:17:38.000000Z","updated_at":"2023-11-12T17:17:38.000000Z"},"created_at":"2023-11-12"}],"created_at":"2023-11-11"},{"id":4,"title":"test test","content":"test text","status":0,"user_id":8,"user":{"id":8,"name":"shady serag","email":"shady@admin.com","role_id":"User","created_at":"2023-11-12"},"comments":[],"created_at":"2023-11-12"}],"links":{"first":"http://127.0.0.1:8000/api/articles?page=1","last":"http://127.0.0.1:8000/api/articles?page=1","prev":null,"next":null},"meta":{"current_page":1,"from":1,"last_page":1,"links":[{"url":null,"label":"&laquo; Previous","active":false},{"url":"http://127.0.0.1:8000/api/articles?page=1","label":"1","active":true},{"url":null,"label":"Next &raquo;","active":false}],"path":"http://127.0.0.1:8000/api/articles","per_page":20,"to":3,"total":3}}
     */
    public function index(ArticleRepository $articleRepository) :AnonymousResourceCollection
    {
        return $articleRepository->searchFromRequest(request());
    }
}