<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Models\Article;
use App\Services\CommentService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CommentController extends Controller
{
    /**
     * Show the form for creating a new resource.
     * 
     * @param int $article_id
     *
     * @return View
     */
    public function create(int $article_id): View
    {
        // Initialize comment and title variables
        $title = trans('admin.add_comment');

        // Pass the comment and title to the view
        return view("admin.comment.form", compact("title", "article_id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $article_id
     * @param  \App\Http\Requests\CommentRequest  $request
     * @param  \App\Services\CommentService  $commentService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(int $article_id, CommentRequest $request, CommentService $commentService): RedirectResponse
    {
        // Create a new comment using the validated data
        $commentService->execute($request->validated(), $article_id);

        // Flash a success message to the session
        session()->flash('success', trans("admin.add Successfully"));

        // Redirect the user to the create form again
        return redirect()->route('admin.comments.create', $article_id);
    }
}