@extends('admin.layouts.app')


@section('content')
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
        <h3 class="content-header-title mb-0 d-inline-block">{{ $title }}</h3>
        <div class="row breadcrumbs-top d-inline-block">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ aurl("/articles/create") }}">{{ trans('admin.add article') }}</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="content-body">
      <!-- HTML5 export buttons table -->
      <!-- Column selectors table -->
      <section id="column-selectors">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">{{ $title }}</h4>
                <form id="search" method="get" >
                  <br />
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title">{{ trans('validation.attributes.title') }}</label>
                        <input type="text" id="title" placeholder="{{ trans('validation.attributes.title') }}" value="{{ request()->input('filter.title') ?? '' }}" class="form-control" style="display:inline;" name="filter[title]" />                  
                      </div>
                      <div class="col-md-3">
                        <label for="content">{{ trans('validation.attributes.content') }}</label>
                        <input type="text" id="content" placeholder="{{ trans('validation.attributes.content') }}" value="{{ request()->input('filter.content') ?? '' }}" class="form-control" style="display:inline;" name="filter[content]" />                  
                      </div>
                      <div class="col-md-2">
                        <label for="roles">{{ trans('admin.status') }}</label>
                        <select name="filter[status]" class="form-control" id="roles">
                          <option value="" {{ request()->input('filter.status') === null ? "selected" : ""  }}>{{ trans('admin.choose') }}</option>
                          <option value="1" {{ request()->input('filter.status') == 1 ? "selected" : ""  }}>{{ trans("admin.active") }}</option>
                          <option value="0" {{ request()->input('filter.status') === '0' ? "selected" : ""  }}>{{ trans("admin.inactive") }}</option>
                        </select>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-success btn-min-width mr-1 mb-1" type="submit" style="display:inline; margin:25px auto;">{{ trans('admin.search') }}</button>
                      </div>
                      <div class="col-md-2">
                        <br>
                        <a class="btn btn-success btn-min-width mr-1 mb-1" href="{{ route("admin.articles.index") }}" style="display:block; margin:3px;">{{ trans('admin.reset_search') }}</a>
                      </div>
                    </div>
                  </div>
                </form>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>{{ trans('validation.attributes.title') }}</th>
                        <th>{{ trans('validation.attributes.status') }}</th>
                        <th>{{ trans('admin.user') }}</th>
                        <th>{{ trans('admin.created_at') }}</th>
                        <th>{{ trans('admin.actions') }}</th>
                        @if (auth()->user()->role_id == 2)
                          <th>{{ trans("admin.active") }}</th>
                        @endif

                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($articles as $article)
                            <tr id="show_{{ $article->id }}">
                                <td>{{ $article->title }}</td>
                                <td>
                                  @if ($article->status == 0)
                                      <span class="text-danger font-weight-bold">{{ trans("admin.inactive") }}</span>
                                  @else
                                      <span class="text-success font-weight-bold">{{ trans("admin.active") }}</span>
                                  @endif
                                </td>
                                <td>{{ $article->user?->name }}</td>
                                <td>{{ $article->created_at ? date("Y-m-d H:i", strtotime($article->created_at)) : "" }}</td>
                                <td>
                                  <a href="{{ aurl('/comments/create/'.$article->id) }}" class="btn btn-primary btn-min-width mr-1 mb-1"><i
                                        class="ft-comment"></i> {{ trans('admin.add_comment') }}</a>
                                    <a href="{{ aurl('/articles/'.$article->id."/edit") }}" class="btn btn-secondary btn-min-width mr-1 mb-1"><i
                                        class="ft-edit"></i> {{ trans('admin.edit') }}</a>
                                    <input type="hidden" value="{{ csrf_token() }}" id="csrf_token" />
                                    <button class="btn btn-danger btn-min-width btn-glow mr-1 mb-1" onclick="DeleteRow({{$article->id}})"><i
                                            class="ft-delete"></i> {{ trans('admin.delete') }}</button>
                                </td>
                                @if (auth()->user()->role_id == 2 && $article->status == 0)
                                  <td><a href="{{ route('admin.article.active', ['article_id' => $article->id]) }}" class="btn btn-success">{{ trans("admin.active") }}</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
          <div class="pagination" style="margin:10px auto">
            {{ $articles->appends(request()->except('page'))->render('vendor.pagination.bootstrap-4') }}
          </div>

        </div>
      </section>
      <!--/ Column selectors table -->
</div>


@endsection
@section('scripts')
<script>
  function DeleteRow(id) {
    	           event.preventDefault();
                   swal({
           		    title: "Are you sure?",
           		    text: "You will not be able to recover this imaginary file!",
           		    icon: "warning",
           		    buttons: {
                           cancel: {
                               text: "No, cancel",
                               value: null,
                               visible: true,
                               className: "",
                               closeModal: false,
                           },
                           confirm: {
                               text: "Yes, delete it!",
                               value: true,
                               visible: true,
                               className: "",
                               closeModal: false
                           }
           		    }
           		})
           		.then((isConfirm) => {
           		    if (isConfirm) {
                        $.ajax({
                            type: 'delete',
                             url: "{{aurl("/articles/")}}"+'/'+id,
                            data: {
                                '_token': $('#csrf_token').val(),
                            },

                        });
                        $('#show_' + id).remove();
           		        swal("Deleted!", "Your data has been deleted.", "success");
           		    } else {
           		        swal("Cancelled", "Your data is safe", "error");
           		    }
           		});

            }

</script>
@endsection
