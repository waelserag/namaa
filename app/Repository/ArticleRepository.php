<?php

namespace App\Repository;

use App\Models\Article;
use App\Resources\ArticleResource;
use Symfony\Component\HttpFoundation\Request;
use EloquentBuilder;

class ArticleRepository
{
    public function searchFromRequest(Request $request)
    {
        $articles = Article::with("user", "comments");
        EloquentBuilder::model($articles)->filters($request->filter)->thenApply(); // Use EloquentBuilder Package
        $count_rows = $request->paginate ? $request->paginate : 20; // Add Number of rows in page
        return ArticleResource::collection($articles->paginate($count_rows));
    }
}
