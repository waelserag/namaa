<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\ArticleService;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;

class ArticleServiceTest extends TestCase
{
    /** @var ArticleService */
    private $articleService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->articleService = new ArticleService();
    }

    protected function authenticateUser(): void
    {
        Auth::loginUsingId(1);
    }

    /** @test */
    public function it_can_create_an_article()
    {
        $this->authenticateUser();

        $requestData = [
            "title"   => "test title",
            "content" => "test content",
        ];

        $createdArticle = $this->articleService->execute($requestData);

        $this->assertInstanceOf(Article::class, $createdArticle);
        $this->assertEquals(auth()->id(), $createdArticle->user_id);
        $this->assertEquals('test title', $createdArticle->title);
    }

    /** 
     * @test 
     * @dataProvider updateArticleDataProvider
     */
    public function it_can_update_an_article($originalTitle, $newTitle)
    {
        $this->authenticateUser();

        // Create a test article first
        $existingArticle = Article::create([
            "title"   => $originalTitle,
            "content" => "test content",
            "user_id" => auth()->id(),
        ]);

        $requestData = [
            'title'   => $newTitle,
            "content" => "test content",
        ];

        $updatedArticle = $this->articleService->execute($requestData, $existingArticle);

        $this->assertInstanceOf(Article::class, $updatedArticle);
        $this->assertEquals($newTitle, $updatedArticle->title);
    }

    public static function updateArticleDataProvider()
    {
        return [
            ['Original Title', 'Updated Title'],
            ['test Title', 'Updated Title'],
        ];
    }


    /** @test */
    public function it_can_delete_an_article()
    {
        $this->authenticateUser();

        // Create a test article first
        $article = Article::create([
            "title"   => "test title",
            "content" => "test content",
            "user_id" => auth()->id(),
        ]);

        $this->articleService->delete($article->id);

        $this->assertDatabaseMissing('articles', ['id' => $article->id]);
    }
}
