<?php

namespace App\Repository;

use App\Models\User;
use App\Resources\UserResource;
use Symfony\Component\HttpFoundation\Request;
use EloquentBuilder;

class UserRepository
{
    public function searchFromRequest(Request $request)
    {
        $users = User::query();
        EloquentBuilder::model($users)->filters($request->filter)->thenApply(); // Use EloquentBuilder Package
        $count_rows = $request->paginate ? $request->paginate : 20; // Add Number of rows in page
        return UserResource::collection($users->paginate($count_rows));
    }
}
