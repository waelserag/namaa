<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">


      <li class="nav-item"><a href="{{ aurl('/dashboard') }}"><i class="la la-home"></i>
            <span class="menu-title" data-i18n="nav.dash.main">{{ trans('admin.dashboard') }}</span></a>
      </li>

      <li class=" navigation-header">
        <span data-i18n="nav.category.support">{{ trans('admin.Menu') }}</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"
          data-placement="right" data-original-title="Support"></i>
      </li>

      <!-- Users -->
      <li><a class="menu-item" href="#" data-i18n="nav.templates.horz.main"><i class="fa fa-user"></i><span class="menu-title" data-i18n="nav.templates.main">{{ trans("admin.users") }}</span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="{{ aurl('/users') }}" data-i18n="nav.templates.horz.classic">{{ trans("admin.show") }}</a></li>
            <li><a class="menu-item" href="{{ aurl('/users/create') }}" data-i18n="nav.templates.horz.top_icon">{{ trans("admin.add") }}</a></li>
          </ul>
      </li>

      <!-- Articles -->
      <li><a class="menu-item" href="#" data-i18n="nav.templates.horz.main"><i class="fa fa-newspaper"></i><span class="menu-title" data-i18n="nav.templates.main">{{ trans("admin.articles") }}</span></a>
          <ul class="menu-content">
            <li><a class="menu-item" href="{{ aurl('/articles') }}" data-i18n="nav.templates.horz.classic">{{ trans("admin.show") }}</a></li>
            <li><a class="menu-item" href="{{ aurl('/articles/create') }}" data-i18n="nav.templates.horz.top_icon">{{ trans("admin.add") }}</a></li>
          </ul>
      </li>

    </ul>

  </div>
</div>
