<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name'     => "Admin",
            'email'    => "admin@admin.com",
            'password' => bcrypt("@admin@123@"),
            'role_id'  => 1,
        ]);
    }
}
