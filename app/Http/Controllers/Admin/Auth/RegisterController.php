<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RegisterController extends Controller
{
    /**
     * Display the register form.
     *
     * @return View
     */
    public function index()
    {
        return view('admin.auth.register');
    }

    /**
     * Handle the register request.
     *
     * @param  RegisterRequest  $request
     * @return RedirectResponse
     */
    public function register(RegisterRequest $request)
    {
        // Validate the registration data
        $data = $request->validated();

        // Create a new user
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        // Redirect the user to the desired location after registration
        return redirect()->intended('admin/dashboard');
    }
}