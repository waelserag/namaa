<?php

use App\Http\Controllers\Admin\ArticleStatusController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Auth\RegisterController;
use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Auth
    //Login
    Route::get('/login', [LoginController::class, 'index'])->name('admin.login');
    Route::post('/login', [LoginController::class, 'login']);

    //Register
    Route::get('/register', [RegisterController::class, 'index'])->name('admin.register');
    Route::post('/register', [RegisterController::class, 'register']);

    //Logout
    Route::get('/logout', [LoginController::class, 'logout'])->name('admin.logout');

//Dashboard
Route::middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard', [DashboardController::class, 'index']);

    //Users
    Route::resource('/users', '\App\Http\Controllers\Admin\UserController');

    //Articles
    Route::resource('/articles', '\App\Http\Controllers\Admin\ArticleController',["as"=>"admin"]);

    //Comments
    Route::get('/comments/create/{article_id}', [CommentController::class, 'create'])->name('admin.comments.create');
    Route::post('/comments/{article_id}', [CommentController::class, 'store'])->name('admin.comments.store');

    //Edit Profile
    Route::get('/profile/edit', [ProfileController::class, 'edit']);

    //Active Articles
    Route::get('/article/active/{article_id}', [ArticleStatusController::class, 'update'])->name("admin.article.active")->middleware("reviewer");
});